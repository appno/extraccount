<?php

namespace Drupal\extraccount\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ActivateUserForm.
 *
 * @package Drupal\extraccount\Form
 */
class ActivateUserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'activate_user_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Name of the user you want to activate.'),
      '#maxlength' => 64,
      '#size' => 64,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Provides feedback if user cannot be activated.
    $name = $form_state->getValue('name');
    $user = user_load_by_name($name);
    if ($user == FALSE) {
      $form_state->setErrorByName('name', $this->t('The account @name is not blocked and cannot be activated', ['@name' => $name]));
    }
    if ($user) {
      if ($user->isActive()) {
        $form_state->setErrorByName('name', $this->t('The account @name is not blocked and cannot be activated', ['@name' => $name]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Sends activation confirmation.
    $name = $form_state->getValue('name');
    $user = user_load_by_name($name);
    \Drupal::service('plugin.manager.mail')
      ->mail('extraccount', 'activation_confirmation', $user->getEmail(), $user->getPreferredLangcode(),
        [
          'account' => $user,
        ]
      );
    drupal_set_message($this->t('The activation email has been sent to user @user.', ['@user' => $user->getAccountname()]), 'status');
  }

}
