<?php

namespace Drupal\extraccount\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\extraccount\Form
 */
class ExtraccountAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'extraccount.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('extraccount.settings');
    $email_token_help = $this->t('Available variables are: [site:name], [site:url], [user:display-name], [user:account-name], [user:mail], [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url], [user:cancel-url] and [user:activate-url]');

    // Defines the interval field. This is the time that an account stays
    // active. When this time has passed accounts will be blocked.
    $form['interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reactivation interval'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('interval'),
      '#description' => $this->t('Number of days after which an account must be activated.'),
    ];

    // Defines the list of domains that are considered internal and that user
    // accounts will be filtered on.
    $whitelist = implode("\n", $config->get('whitelist'));
    $form['whitelist'] = [
      '#type' => 'textarea',
      '#title' => t('Whitelist'),
      '#description' => t('List of domains that are whitelisted for user registration. Put each domain on a new line.'),
      '#default_value' => $whitelist,
    ];

    // Defines activation time out field. This time out is the time that the
    // user is allowed reset the activation before the account will be blocked.
    // This will be used as exipration time for the account activation link.
    $form['activation_timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Activation time out'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('activation_timeout'),
      '#description' => $this->t('Number of days the user has to renew the activation of the account.'),
    ];

    // Define vertical tabs for email content.
    $form['email'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Emails'),
    ];

    // Defines approval request subject and body for sending approval request
    // emails to internal users.
    $form['approval_request'] = [
      '#type' => 'details',
      '#title' => $this->t('Account request approval'),
      '#description' => $this->t('Edit the email messages sent to users that must approve an external account.') . ' ' . $email_token_help,
      '#group' => 'email',
      '#weight' => 10,
    ];
    $form['approval_request']['approval_request_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('approval_request.subject'),
      '#maxlength' => 180,
    ];
    $form['approval_request']['approval_request_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('approval_request.body'),
      '#rows' => 12,
    ];

    // Defines activation confirmation subject and body for sending activation
    // confirmation emails to activated users.
    $form['activation_confirmation'] = [
      '#type' => 'details',
      '#title' => $this->t('Account activation confirmation'),
      '#description' => $this->t('Edit the email messages sent users whose account request was activated.') . ' ' . $email_token_help,
      '#group' => 'email',
      '#weight' => 10,
    ];
    $form['activation_confirmation']['activation_confirmation_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('activation_confirmation.subject'),
      '#maxlength' => 180,
    ];
    $form['activation_confirmation']['activation_confirmation_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('activation_confirmation.body'),
      '#rows' => 12,
    ];

    // Defines block notification subject and body for sending notification
    // emails to users whose accounts have been blocked.
    $form['block_notification'] = [
      '#type' => 'details',
      '#title' => $this->t('Block notification'),
      '#description' => $this->t('Edit the email messages sent to users whose account has been blocked.') . ' ' . $email_token_help,
      '#group' => 'email',
      '#weight' => 10,
    ];
    $form['block_notification']['block_notification_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('block_notification.subject'),
      '#maxlength' => 180,
    ];
    $form['block_notification']['block_notification_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('block_notification.body'),
      '#rows' => 12,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Checks if interval is positive integer.
    $interval = $form_state->getValue('interval');
    if ($interval !== '' && (!is_numeric($interval) || intval($interval) != $interval || $interval <= 0)) {
      $form_state->setErrorByName('', $this->t('@integer is not a valid as reactivation interval', ['@integer' => $interval]));
    }

    // Checks if activation_timeout is positive integer.
    $activation_timeout = $form_state->getValue('activation_timeout');
    if ($activation_timeout !== '' && (!is_numeric($activation_timeout) || intval($activation_timeout) != $activation_timeout || $activation_timeout <= 0)) {
      $form_state->setErrorByName('', $this->t('@integer is not a valid as activation timeout', ['@integer' => $activation_timeout]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Transforms user input of whitelist field into array.
    $whitelist = [];
    foreach (array_values(array_filter(explode(PHP_EOL, $form_state->getValue('whitelist')))) as $item) {
      $whitelist[] = filter_var($item, FILTER_SANITIZE_URL);
    }

    // Saves settings.
    $this->config('extraccount.settings')
      ->set('interval', $form_state->getValue('interval'))
      ->set('whitelist', $whitelist)
      ->set('activation_timeout', $form_state->getValue('activation_timeout'))
      ->set('approval_request.subject', $form_state->getValue('approval_request_subject'))
      ->set('approval_request.body', $form_state->getValue('approval_request_body'))
      ->set('activation_confirmation.subject', $form_state->getValue('activation_confirmation_subject'))
      ->set('activation_confirmation.body', $form_state->getValue('activation_confirmation_body'))
      ->set('block_notification.subject', $form_state->getValue('block_notification_subject'))
      ->set('block_notification.body', $form_state->getValue('block_notification_body'))
      ->save();
  }

}
