<?php

namespace Drupal\extraccount\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Notifies an account that is about to be blocked.
 *
 * @QueueWorker(
 *   id = "block_account_notification_queue",
 *   title = @Translation("Extranet account: notify accounts about to expire"),
 *   cron = {"time" = 60}
 * )
 */
class BlockAccountNotificationQueue extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $user = $data['user_to_expire'];
    \Drupal::service('plugin.manager.mail')
      ->mail('extraccount', 'block_notification', $user->getEmail(), $user->getPreferredLangcode(),
        [
          'account' => $user,
        ]
      );
  }

}
