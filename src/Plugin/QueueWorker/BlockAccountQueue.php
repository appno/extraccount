<?php

namespace Drupal\extraccount\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Blocks expired account.
 *
 * @QueueWorker(
 *   id = "block_account_queue",
 *   title = @Translation("Extranet account: block expired accounts"),
 *   cron = {"time" = 60}
 * )
 */
class BlockAccountQueue extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $user = $data['user'];

    if (extraccount_find_mail_in_whitelist($user->getEmail())) {
      // Blocks the user.
      $user->block();
      $user->save();

      // Logs the action.
      $logger = \Drupal::logger('extraccount');
      $logger->info('The account @name has been blocked.', [
        '@name' => $user->getAccountName(),
      ]);
    }
    else {
      // Logs the action.
      $logger = \Drupal::logger('extraccount');
      $logger->info('The account @name has been deleted.', [
        '@name' => $user->getAccountName(),
      ]);

      // Delete the user.
      user_delete($user->id());
    }
  }
}
