<?php

namespace Drupal\extraccount\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ActivateUserBlock' block.
 *
 * @Block(
 *  id = "activate_user_block",
 *  admin_label = @Translation("Activate user block"),
 * )
 */
class ActivateUserBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()->getForm('Drupal\extraccount\Form\ActivateUserForm');
  }

}
