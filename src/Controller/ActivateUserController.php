<?php

namespace Drupal\extraccount\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Drupal\Component\Utility\Crypt;

/**
 * Class ActivateUserController.
 *
 * @package Drupal\extraccount\Controller
 */
class ActivateUserController extends ControllerBase {

  /**
   * Sets up variables.
   */
  public function __construct() {
    $this->current_user = \Drupal::currentUser();
  }

  /**
   * Sends an activation confirmation mail to the user.
   *
   * The mail contains a url from which the account can be activated.
   *
   * @return string
   *   Feedback for the administrator.
   */
  public function sendMail($uid) {
    $user = User::load($uid);
    \Drupal::service('plugin.manager.mail')
      ->mail('extraccount', 'activation_confirmation', $user->getEmail(), $user->getPreferredLangcode(),
        [
          'account' => $user,
        ]
      );
    drupal_set_message($this->t('The activation email has been sent to @mail.', ['@mail' => $user->getEmail()]), 'status');
    return $this->redirect('user.login');
  }

  /**
   * Activates the user and provides feedback to the administrator.
   *
   * @return string
   *   Feedback for the administrator.
   */
  public function activateUser($uid, $timestamp, $hash) {

    // Loads the user using the uid.
    $user = User::load($uid);

    // Sets up logging.
    $logger = \Drupal::logger('extraccount');

    // Fetches extraccount settings.
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('extraccount.settings');

    // Calculates url exipration.
    $current = REQUEST_TIME;
    $timeout = $config->get('activation_timeout') * 60 * 60 * 24;

    // Provides feedback when the url has expired or has the wrong hash.
    if ($current - $timestamp > $timeout || Crypt::hashEquals($hash, user_pass_rehash($user, $timestamp)) == FALSE) {
      drupal_set_message($this->t('You have used a link that has expired. Please request a new one via the login form.'), 'error');
      return $this->redirect('user.login');
    }

    // Activates the account and sets activation date to today.
    $user->activate();
    $user->field_extraccount_activation->value = time();
    $user->save();
//    drupal_set_message($this->t('The account @name has been activated.', ['@name' => $user->getAccountName()]), 'status');
//    $logger->info('The account @name has been activated.', [
//      '@name' => $user->getAccountName(),
//    ]);
//    return $this->redirect('user.login');

    // Logs in if the user is in the whitelist.
    if (extraccount_find_mail_in_whitelist($user->getEmail())) {
      drupal_set_message($this->t('Registration successful. You are now logged in.'));
      user_login_finalize($user);
      return $this->redirect('<front>');
    }

    /*
     * Otherwise only an admin will click the activation link. Because an
     * approver should not be logged in, redirect to the logout page.
     */
//    user_logout();
    drupal_set_message($this->t('Registration of user %email successful.',
      ['%email' => $user->getEmail()]));
    return $this->redirect('<front>');
  }

}
