# Extra account #

The extranet account module provides account approval and reactivation mechanism
for extranets made with Drupal 8.

Git repo: https://bitbucket.org/appno/extraccount

The module does not have any dependencies besides Drupal core.


### Install ###

* Install the module like an oridinary contributed Drupal module. See
https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

### Configure ###

* Go to /admin/config/people/accounts
* Choose *Visitors, but administrator approval is required* in the list
*Who can register accounts?*. This will allow the custom module to intercept
the registration process.
* Go to /admin/config/people/accounts/form-display
* Enable the *Approving contact person* field on the registration form
* Go to /admin/config/people/extraccount to configure the module. See a
description of the settings below.

#### Reactivation interval

The reactivation interval defines after which period the user accounts will be
blocked and reactivation will become necessary. For example if the interval is
set to 365 days and a user register on July 1, 2017, then his account will be
locked July 1, 2018.

#### Whitelist

The whitelist is the list of domain names against which the accounts will be
checked. Users that register with an email address that matches a domain name
in the whitelist can create an activated account. If there is no match a blocked
account will be created.
During registration users can enter an approver email address. This is matched
against the whitelist as well.
Also users that match the whitelist will be notified that their account is
going to be blocked.

#### Activation time-out
The activation time-out is the time that user are allowed to activate their
accounts before it is being blocked. In other words it is the time before the
activation link expires.

#### Emails

##### Account request approval
*Account request approval* subject and body defines the email that will be sent
to an approver. It should contain the activation link (token:
``[user:activate-url]``)

##### Account activation confirmation
*Account activation* confirmation subject and body defines the email that will
be sent to a user after the account is activated.

##### Block notification
*Block notification* subject and body defines the email that will be sent to a
user before the account is being blocked. It should contain the activation link
(token: ``[user:activate-url]``)

### More info ###

* nl.info@wunderkraut.com
